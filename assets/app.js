/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
// import './styles/app.css';

/******** HTMX **** */
import 'htmx.org';
window.htmx = require('htmx.org');

/** NAVBAR COLLAPSE **/
const collapseEl = document.getElementById('example-collapse-navbar')
const navTogglerEl = document.getElementById('nav-toggler')
navTogglerEl.addEventListener('click', (e) => {
    e.preventDefault()
    if (collapseEl.classList.contains('hidden')) {
        collapseEl.classList.remove('hidden')
        collapseEl.classList.add('block')        
    }
    else {
        collapseEl.classList.remove('block')
        collapseEl.classList.add('hidden')       
    }
})