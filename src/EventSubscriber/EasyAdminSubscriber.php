<?php

namespace App\EventSubscriber;

use App\Entity\Invitation;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Uid\Uuid;

class EasyAdminSubscriber implements EventSubscriberInterface
{
    public static function getSubscribedEvents(): array
    {
        return [
            BeforeEntityPersistedEvent::class => ['setUuuid'],
        ];
    }

    public function setUuuid(BeforeEntityPersistedEvent $event): void
    {
        $entity = $event->getEntityInstance();
        // dd($event, $entity);
        if ($entity instanceof Invitation) {
            $entity->setUuuid(Uuid::v4());
        }

        return;
    }
}
