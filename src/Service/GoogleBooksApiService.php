<?php

declare(strict_types=1);

namespace App\Service;

use Symfony\Contracts\HttpClient\HttpClientInterface;

class GoogleBooksApiService
{
    public function __construct(
        private readonly HttpClientInterface $googleBooksClient
    ) {
    }

    /**
     * Search for books.
     *
     * @return array <mixed>
     */
    public function search(string $search): array
    {
        if (strlen($search) < 3) {
            return [];
        }

        return $this->makeRequest('GET', 'https://www.googleapis.com/books/v1/volumes', [
            'query' => [
                'q' => $search,
            ],
        ]);
    }

    /**
     * Get a book.
     *
     * @return array <mixed>
     */
    public function get(string $id): array
    {
        return $this->makeRequest('GET', 'https://www.googleapis.com/books/v1/volumes/'.$id);
    }

    /**
     * Make a request to Google Books API.
     *
     * @param array <mixed> $options
     *
     * @return array <mixed>
     */
    private function makeRequest(string $method, string $url, array $options = []): array
    {
        $response = $this->googleBooksClient->request($method, $url, $options);

        return $response->toArray();
    }
}
