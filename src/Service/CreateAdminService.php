<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class CreateAdminService
{
    public function __construct(
        private readonly UserRepository $userRepository,
        private readonly UserPasswordHasherInterface $passwordHasher
    ) {
    }

    public function create(string $email, string $password): void
    {
        $user = $this->userRepository->findOneBy(['email' => $email]);
        if (!$user) {
            // TODO: Check if email is valid
            // Create the admin user from the command line
            $user = new User();
            $user->setEmail($email);
            $user->setPassword($this->passwordHasher->hashPassword($user, $password));
        }
        // Add the ROLE_ADMIN from the command line for an existing user. The email and password are the same
        $user->setRoles(['ROLE_ADMIN']);

        $this->userRepository->save($user, true);
    }
}
