<?php

namespace App\Tests\Entity;

use App\Entity\Author;
use App\Entity\Book;
use App\Entity\Publisher;
use App\Entity\UserBook;
use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Framework\TestCase;

class BookTest extends TestCase
{
    public function testConstruct()
    {
        $book = new Book();

        $this->assertNull($book->getId());
        $this->assertNull($book->getGoogleBooksId());
        $this->assertNull($book->getTitle());
        $this->assertNull($book->getSubtitle());
        $this->assertNull($book->getPublishDate());
        $this->assertNull($book->getDescription());
        $this->assertNull($book->getIsbn10());
        $this->assertNull($book->getIsbn13());
        $this->assertNull($book->getPageCount());
        $this->assertNull($book->getSmallThumbnail());
        $this->assertNull($book->getThumbnail());
        $this->assertInstanceOf(ArrayCollection::class, $book->getAuthors());
        $this->assertCount(0, $book->getAuthors());
        $this->assertInstanceOf(ArrayCollection::class, $book->getPublishers());
        $this->assertCount(0, $book->getPublishers());
        $this->assertInstanceOf(ArrayCollection::class, $book->getUserBooks());
        $this->assertCount(0, $book->getUserBooks());
    }

    public function testGettersAndSetters(): void
    {
        $book = new Book();
        $this->assertNull($book->getId());
        $book->setGoogleBooksId('123');
        $this->assertSame('123', $book->getGoogleBooksId());
        $book->setTitle('The Great Gatsby');
        $this->assertSame('The Great Gatsby', $book->getTitle());
        $book->setSubtitle('A Novel');
        $this->assertSame('A Novel', $book->getSubtitle());
        $book->setPublishDate(new \DateTime('2022-01-01'));
        $this->assertSame('2022-01-01', $book->getPublishDate()->format('Y-m-d'));
        $book->setDescription('A description');
        $this->assertSame('A description', $book->getDescription());
        $book->setIsbn10('123');
        $this->assertSame('123', $book->getIsbn10());
        $book->setIsbn13('123');
        $this->assertSame('123', $book->getIsbn13());
        $book->setPageCount(123);
        $this->assertSame(123, $book->getPageCount());
        $book->setSmallThumbnail('123');
        $this->assertSame('123', $book->getSmallThumbnail());
        $book->setThumbnail('123');
        $this->assertSame('123', $book->getThumbnail());
        $author = new Author();
        $book->addAuthor($author);
        $this->assertSame($author, $book->getAuthors()->first());
        $publisher = new Publisher();
        $book->addPublisher($publisher);
        $this->assertSame($publisher, $book->getPublishers()->first());
        $userBook = new UserBook();
        $book->addUserBook($userBook);
        $this->assertSame($userBook, $book->getUserBooks()->first());
    }

    public function testSetGoogleBooksId()
    {
        $book = new Book();
        $googleBooksId = '123456';

        $book->setGoogleBooksId($googleBooksId);
        $this->assertSame($googleBooksId, $book->getGoogleBooksId());
    }


    public function testSetTitle()
    {
        $book = new Book();
        $title = 'The Great Gatsby';

        $book->setTitle($title);
        $this->assertSame($title, $book->getTitle());
    }

    public function testSetSubtitle()
    {
        $book = new Book();
        $subtitle = 'A Novel';

        $book->setSubtitle($subtitle);
        $this->assertSame($subtitle, $book->getSubtitle());
    }

    public function testSetPublishDate()
    {
        $book = new Book();
        $publishDate = new \DateTime('2022-01-01');

        $book->setPublishDate($publishDate);
        $this->assertSame($publishDate, $book->getPublishDate());
    }

    public function testSetDescription()
    {
        $book = new Book();
        $description = 'The story primarily concerns the young and mysterious millionaire Jay Gatsby';

        $book->setDescription($description);
        $this->assertSame($description, $book->getDescription());
    }

    public function testSetIsbn10()
    {
        $book = new Book();
        $isbn10 = '0141182636';

        $book->setIsbn10($isbn10);
        $this->assertSame($isbn10, $book->getIsbn10());
    }

    public function testSetIsbn13()
    {
        $book = new Book();
        $isbn13 = '978-0141182636';

        $book->setIsbn13($isbn13);
        $this->assertSame($isbn13, $book->getIsbn13());
    }

    public function testSetPageCount()
    {
        $book = new Book();
        $pageCount = 180;

        $book->setPageCount($pageCount);
        $this->assertSame($pageCount, $book->getPageCount());
    }

    public function testSetSmallThumbnail()
    {
        $book = new Book();
        $smallThumbnail = 'https://books.google.com/books/content?id=...';

        $book->setSmallThumbnail($smallThumbnail);
        $this->assertSame($smallThumbnail, $book->getSmallThumbnail());
    }

    public function testSetThumbnail()
    {
        $book = new Book();
        $thumbnail = 'https://books.google.com/books/content?id=...';

        $book->setThumbnail($thumbnail);
        $this->assertSame($thumbnail, $book->getThumbnail());
    }

    public function testAddAndRemoveAuthor()
    {
        $book = new Book();
        $author = new Author();

        $book->addAuthor($author);
        $this->assertInstanceOf(ArrayCollection::class, $book->getAuthors());
        $this->assertTrue($book->getAuthors()->contains($author));

        $book->removeAuthor($author);
        $this->assertFalse($book->getAuthors()->contains($author));
    }

    public function testAddAndRemovePublisher()
    {
        $book = new Book();
        $publisher = new Publisher();

        $book->addPublisher($publisher);
        $this->assertInstanceOf(ArrayCollection::class, $book->getPublishers());
        $this->assertTrue($book->getPublishers()->contains($publisher));

        $book->removePublisher($publisher);
        $this->assertFalse($book->getPublishers()->contains($publisher));
    }

    public function testAddAndRemoveUserBook()
    {
        $book = new Book();
        $userBook = new UserBook();

        $book->addUserBook($userBook);
        $this->assertInstanceOf(ArrayCollection::class, $book->getUserBooks());
        $this->assertTrue($book->getUserBooks()->contains($userBook));
        $this->assertSame($book, $userBook->getBook());

        $book->removeUserBook($userBook);
        $this->assertFalse($book->getUserBooks()->contains($userBook));
        $this->assertNull($userBook->getBook());
    }
}
