<?php

namespace App\Tests\Entity;

use App\Entity\User;
use App\Entity\UserBook;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class UserTest extends TestCase
{
    public function testConstruct()
    {
        $user = new User();

        $this->assertNull($user->getId());
        $this->assertNull($user->getEmail());
        $this->assertIsArray($user->getRoles());
        $this->assertCount(1, $user->getRoles());
        $this->assertSame('ROLE_USER', $user->getRoles()[0]);
        $this->assertNull($user->getPseudo());
        $this->assertCount(0, $user->getUserBooks());
    }

    public function testSetAndGetEmail()
    {
        $user = new User();
        $email = 'test@example.com';

        $this->assertNull($user->getEmail());

        $user->setEmail($email);
        $this->assertSame($email, $user->getEmail());
        $this->assertSame($email, $user->getUserIdentifier());
    }

    public function testGetRoles()
    {
        $user = new User();
        $roles = ['ROLE_ADMIN'];

        $this->assertIsArray($user->getRoles());
        $this->assertCount(1, $user->getRoles());
        $this->assertSame('ROLE_USER', $user->getRoles()[0]);

        $user->setRoles($roles);
        $this->assertIsArray($user->getRoles());
        $this->assertCount(2, $user->getRoles());
        $this->assertSame($roles[0], $user->getRoles()[0]);
        $this->assertSame('ROLE_USER', $user->getRoles()[1]);
    }

    public function testSetAndGetPassword()
    {
        $user = new User();
        $password = 'password';

        $user->setPassword($password);
        $this->assertSame($password, $user->getPassword());
    }

    public function testEraseCredentials()
    {
        $user = new User();

        $this->assertInstanceOf(PasswordAuthenticatedUserInterface::class, $user);
        $this->assertNull($user->eraseCredentials());
    }

    public function testSetAndGetPseudo()
    {
        $user = new User();
        $pseudo = 'JohnDoe';

        $this->assertNull($user->getPseudo());

        $user->setPseudo($pseudo);
        $this->assertSame($pseudo, $user->getPseudo());
    }

    public function testAddAndRemoveUserBook()
    {
        $user = new User();
        $userBook = new UserBook();

        $user->addUserBook($userBook);
        $this->assertInstanceOf(\Doctrine\Common\Collections\ArrayCollection::class, $user->getUserBooks());
        $this->assertTrue($user->getUserBooks()->contains($userBook));
        $this->assertSame($user, $userBook->getReader());

        $user->removeUserBook($userBook);
        $this->assertFalse($user->getUserBooks()->contains($userBook));
        $this->assertNull($userBook->getReader());
    }
}

    