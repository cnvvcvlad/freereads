<?php

use App\Entity\Trait\IdNameTrait;
use PHPUnit\Framework\TestCase;

class IdNameTraitTest extends TestCase
{

    public function testCanGetId(): void
    {
        $idName = $this->getObjectForTrait(IdNameTrait::class);
        $id = 1;
        $idName->setId($id);
        $this->assertEquals($id, $idName->getId());
    }

    public function testCanSetAndGetName(): void
    {
        $idName = $this->getObjectForTrait(IdNameTrait::class);
        $name = 'John Doe';
        $idName->setName($name);
        $this->assertEquals($name, $idName->getName());
    }
}
