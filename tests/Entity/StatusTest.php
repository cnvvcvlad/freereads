<?php

namespace App\Tests\Entity;

use App\Entity\Status;
use App\Entity\UserBook;
use PHPUnit\Framework\TestCase;

class StatusTest extends TestCase
{
    public function testAddAndRemoveUserBook()
    {
        $status = new Status();
        $userBook = new UserBook();

        $status->addUserBook($userBook);
        $this->assertInstanceOf(\Doctrine\Common\Collections\ArrayCollection::class, $status->getUserBooks());
        $this->assertTrue($status->getUserBooks()->contains($userBook));
        $this->assertSame($status, $userBook->getStatus());

        $status->removeUserBook($userBook);
        $this->assertFalse($status->getUserBooks()->contains($userBook));
        $this->assertNull($userBook->getStatus());
    }
}
