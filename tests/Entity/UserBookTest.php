<?php

use App\Entity\UserBook;
use App\Entity\User;
use App\Entity\Book;
use App\Entity\Status;
use PHPUnit\Framework\TestCase;

class UserBookTest extends TestCase
{
    public function testConstruct(): void
    {
        $userBook = new UserBook();

        $this->assertNull($userBook->getId());
        $this->assertNull($userBook->getCreatedAt());
        $this->assertNull($userBook->getUpdatedAt());
        $this->assertNull($userBook->getComment());
        $this->assertNull($userBook->getRating());
        $this->assertNull($userBook->getReader());
        $this->assertNull($userBook->getBook());
        $this->assertNull($userBook->getStatus());
    }

    public function testSetAndGetCreatedAt(): void
    {
        $userBook = new UserBook();
        $createdAt = new \DateTimeImmutable();

        $userBook->setCreatedAt($createdAt);
        $this->assertSame($createdAt, $userBook->getCreatedAt());
    }

    public function testSetAndGetUpdatedAt(): void
    {
        $userBook = new UserBook();
        $updatedAt = new \DateTimeImmutable();

        $userBook->setUpdatedAt($updatedAt);
        $this->assertSame($updatedAt, $userBook->getUpdatedAt());
    }
    
    public function testCanCreateUserBookInstance(): void
    {
        $userBook = new UserBook();
        $this->assertInstanceOf(UserBook::class, $userBook);
    }

    public function testCanSetAndGetComment(): void
    {
        $userBook = new UserBook();
        $comment = 'This is a great book!';
        $userBook->setComment($comment);
        $this->assertEquals($comment, $userBook->getComment());
    }

    public function testCanSetAndGetRating(): void
    {
        $userBook = new UserBook();
        $rating = 4;
        $userBook->setRating($rating);
        $this->assertEquals($rating, $userBook->getRating());
    }

    public function testCanSetAndGetReader(): void
    {
        $userBook = new UserBook();
        $reader = new User();
        $userBook->setReader($reader);
        $this->assertEquals($reader, $userBook->getReader());
    }

    public function testCanSetAndGetBook(): void
    {
        $userBook = new UserBook();
        $book = new Book();
        $userBook->setBook($book);
        $this->assertEquals($book, $userBook->getBook());
    }

    public function testCanSetAndGetStatus(): void
    {
        $userBook = new UserBook();
        $status = new Status();
        $userBook->setStatus($status);
        $this->assertEquals($status, $userBook->getStatus());
    }
}
