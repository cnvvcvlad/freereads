<?php

namespace App\Tests\Entity;

use App\Entity\Book;
use App\Entity\Publisher;
use PHPUnit\Framework\TestCase;

class PublisherTest extends TestCase
{
    public function testAddAndRemoveBook()
    {
        $publisher = new Publisher();
        $book = new Book();

        $publisher->addBook($book);
        $this->assertInstanceOf(\Doctrine\Common\Collections\ArrayCollection::class, $publisher->getBooks());
        $this->assertTrue($publisher->getBooks()->contains($book));
        $this->assertTrue($book->getPublishers()->contains($publisher));

        $publisher->removeBook($book);
        $this->assertFalse($publisher->getBooks()->contains($book));
        $this->assertFalse($book->getPublishers()->contains($publisher));
    }
}
