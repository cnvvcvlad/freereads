<?php
use App\Entity\Author;
use App\Entity\Book;
use PHPUnit\Framework\TestCase;

class AuthorTest extends TestCase
{
    public function testAddBook()
    {
        $author = new Author();
        $book1 = new Book();
        $book2 = new Book();

        $author->addBook($book1);
        $author->addBook($book2);

        $this->assertCount(2, $author->getBooks());
        $this->assertTrue($author->getBooks()->contains($book1));
        $this->assertTrue($author->getBooks()->contains($book2));
    }

    public function testRemoveBook()
    {
        $author = new Author();
        $book1 = new Book();
        $book2 = new Book();

        $author->addBook($book1);
        $author->addBook($book2);

        $author->removeBook($book1);

        $this->assertCount(1, $author->getBooks());
        $this->assertFalse($author->getBooks()->contains($book1));
        $this->assertTrue($author->getBooks()->contains($book2));
    }
}
