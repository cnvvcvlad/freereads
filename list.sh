#Charger les variables d'environnement
source ./.env.local

#Configuration du profil du client S3
aws configure set aws_endpoint $S3_ENDPOINT_URL --profile backup
aws configure set aws_access_key_id $S3_ACCESS_KEY_ID --profile backup
aws configure set aws_secret_access_key $S3_SECRET_ACCESS_KEY --profile backup
aws configure set default.region $S3_REGION --profile backup

#Créer un répertoire pour les logs et les backups s'ils n'existent pas
mkdir -p ./var/log

#Definir le format de la date et du nom de fichier log
DATE=$(date +%Y%m%d_%H%M%S)
LOG_FILE="./var/log/list_objects_$DATE.log"

#Démarrer le log
echo "$DATE: Debut de la liste des objets" >> $LOG_FILE

#Lister les objets dans le bucket
echo "$DATE: Liste des objets" >> $LOG_FILE
aws s3 ls s3://$S3_BUCKET_NAME/ --profile backup --endpoint-url $S3_ENDPOINT_URL >> $LOG_FILE
echo "$DATE: Fin de la liste des objets" >> $LOG_FILE

#Finalisation du log
echo "$DATE: Fin de la liste des objets sauvegardes" >> $LOG_FILE